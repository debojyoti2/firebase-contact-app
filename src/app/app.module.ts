import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { ContactsPage } from '../pages/contacts/contacts';
import { FiredataProvider } from '../providers/firedata/firedata';
import { AddContactsPage } from '../pages/contacts/add-contacts/add-contacts';
import { ContactEditModalPage } from '../pages/contacts/contact-edit-modal/contact-edit-modal';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environment/environment';

import { AngularFirestoreModule } from 'angularfire2/firestore';

@NgModule({
  declarations: [
    MyApp,
    ContactsPage,
    AddContactsPage,
    ContactEditModalPage
  ],
  imports: [
    BrowserModule,
    AngularFirestoreModule, // for database
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(environment.firebase)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ContactsPage,
    AddContactsPage,
    ContactEditModalPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FiredataProvider
  ]
})
export class AppModule {}
