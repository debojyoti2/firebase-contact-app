import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { AddContactsPage } from './add-contacts/add-contacts';
import { ContactEditModalPage } from '../contacts/contact-edit-modal/contact-edit-modal';
import { FiredataProvider } from '../../providers/firedata/firedata';

@Component({
  selector: 'contacts-page',
  templateUrl: 'contacts.html'
})
export class ContactsPage implements OnInit {

  public contacts: any[];
  private fetchedContacts: any[];

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public fireData: FiredataProvider
  ) { }

  ngOnInit() {
    // Subscribe to the data service
    this.fireData.read().subscribe(contacts => {
      this.fetchedContacts = contacts;
      this.contacts = contacts;
    });
    // Request initial load
    this.fireData.sync();
  }

  public addContact(): void {
    this.navCtrl.push(AddContactsPage);
  }

  public editContact(contact): void {
    let editModal = this.modalCtrl.create(
      ContactEditModalPage, {
        'contact': contact
      });
    editModal.present();
  }

  public serachContacts(event): void {
    // // Reinitilize the list
    this.contacts = this.fetchedContacts;

    const searchVal = event.target.value;
    if (searchVal && searchVal.trim() !== '') {
      this.contacts = this.contacts.filter(contact => {
        return contact.name.toLowerCase().includes(searchVal.toLowerCase()) || contact.phone.includes(searchVal);
      }
      )
    }
  }

}
