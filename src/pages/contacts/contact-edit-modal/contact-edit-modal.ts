import { Component } from '@angular/core';
import { NavParams, ViewController, ToastController } from 'ionic-angular';
import { FiredataProvider } from '../../../providers/firedata/firedata';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-contact-edit-modal',
  templateUrl: 'contact-edit-modal.html',
})
export class ContactEditModalPage {

  public contact: any;

  constructor(
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public toastCtrl: ToastController,
    public fireData: FiredataProvider,
    private alertCtrl: AlertController
  ) {
    this.contact = navParams.data.contact;
  }

  public update():void {
    this.fireData.update(this.contact);
    this.viewCtrl.dismiss();
  }

  private showToast(): void {
    const toast = this.toastCtrl.create({
      message: 'Removed ' + this.contact.name,
      duration: 5000
    });
    toast.present();
  }

  public dismiss() {
    this.viewCtrl.dismiss();
  }

  public deleteConfirm(): void {
    let alert = this.alertCtrl.create({
      title: 'Confirm Delete',
      message: 'Do you want to delete ' + this.contact.name + ' ?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            // Cancel Clicked
          }
        },
        {
          text: 'Delete',
          handler: () => {
            // Delete clicked
            this.fireData.delete(this.contact);
            this.showToast();
            this.viewCtrl.dismiss();
          }
        }
      ]
    });
    alert.present();
  }



}
