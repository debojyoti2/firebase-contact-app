import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FiredataProvider } from '../../../providers/firedata/firedata';

@Component({
  selector: 'page-add-contacts',
  templateUrl: 'add-contacts.html',
})
export class AddContactsPage {

  public addContactForm: FormGroup;
  public submitted = false;
  public valid = false;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public toastCtrl: ToastController,
    public fireData: FiredataProvider
  ) { }

  ngOnInit() {
    this.addContactForm = this.formBuilder.group({
      name: ['', Validators.required],
      phone: ['', [Validators.required, Validators.minLength(10)]]
    });
  }

  public validate(): void {
    this.submitted = true;
    if (!this.addContactForm.invalid) {
      // If all validations are passed successfully
      this.valid = true;
      return;
    } else {
      this.valid = false;
    }
  }

  public add(): void {
    let contact = this.addContactForm.value;
    this.fireData.add(contact);
    this.showToast(contact);
    this.navCtrl.pop();
  }

  private showToast(contact) {
    const toast = this.toastCtrl.create({
      message: contact.name + ' added successfully',
      duration: 3000
    });
    toast.present();
  }
  

  get f() { return this.addContactForm.controls; }

}
