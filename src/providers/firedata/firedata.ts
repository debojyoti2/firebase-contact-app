import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class FiredataProvider {

  private contacts: any = [];
  private contactsResponse = new Subject<any>();

  constructor(public db: AngularFirestore) { }

  public sync(): void {
    this.db.collection('contacts').snapshotChanges().subscribe(actions => {
      this.contacts = [];
      actions.forEach(action => {
        this.contacts.push({
          'firestoreDocId': action.payload.doc.id,
          'name': action.payload.doc.data()['name'],
          'phone': action.payload.doc.data()['phone'],
        })
      });
      this.contactsResponse.next(this.contacts);
    })
  }

  public read(): Observable<any> {
    return this.contactsResponse.asObservable();
  }

  public update(contact): void {
    this.db.collection('contacts').doc(contact.firestoreDocId).set({
      'name': contact.name,
      'phone': contact.phone
    })
      .catch(function (error) {
        console.error('Error writing document: ', error);
      });
  }

  public delete(contact): void {
    this.db.collection('contacts').doc(contact.firestoreDocId).delete();
  }

  public add(contact): void {
    this.db.collection('contacts').add(contact)
      .catch(function (error) {
        console.error('Error writing document: ', error);
      });
  }


}
